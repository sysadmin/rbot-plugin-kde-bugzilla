[![Build Status](https://travis-ci.org/apachelogger/kde-bugzilla-rbot-plugin.svg?branch=master)](https://travis-ci.org/apachelogger/kde-bugzilla-rbot-plugin)

# Installation

- Clone anywhere
- Install dependencies
  - When using Bundler: `bundle install --without=development`
  - Manually: `gem install finer_struct && gem install faraday`
- Add clone path to `~/.rbot/conf.yaml`
```
plugins.path:
  - /home/me/src/git/rbot-bugzilla
```
- Restart bot

# Blacklists

The plugin supports configuring a list of channels to be blacklisted. This
inhibits either all message handling or specific message handling.

- `bugzilla.blacklist` disables all message handling
- `bugzilla.url_blacklist` disables handling of URLs (keywords remain
  active - e.g. `https://bugs.kde.org/123` is not handled but `bug 123` still is)
